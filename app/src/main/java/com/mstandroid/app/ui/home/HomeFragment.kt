package com.mstandroid.app.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.mstandroid.app.BR
import com.mstandroid.app.R
import com.mstandroid.app.components.vm.VmList
import com.promix.baelui.bind.binder.ComposeItemBuilder
import com.promix.baelui.ext.bindItems
import com.promix.baelui.ext.bindView
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerView.bindView(
            ComposeItemBuilder.Builder().addItemBinder(
                VmList::class,
                BR.model,
                R.layout.item_list
            ).build()
        )

        val items = mutableListOf<VmList>()
        for (i in 1..10) {
            items.add(VmList("Title $i"))
        }
        recyclerView.bindItems(items)
    }
}