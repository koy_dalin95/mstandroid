package com.mstandroid.app.ui.auth

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import com.mstandroid.app.R
import com.mstandroid.app.components.base.BaseBindFragment
import com.mstandroid.app.databinding.FragmentLoginBinding
import com.mstandroid.app.ui.MainActivity
import kotlinx.android.synthetic.main.fragment_login.*

class LoginFragment : BaseBindFragment<FragmentLoginBinding>(R.layout.fragment_login) {
    private val viewModel by viewModels<LoginViewModel>()

    override fun onBindingView(binding: FragmentLoginBinding) {
        binding.let {
            it.viewModel = viewModel
            it.lifecycleOwner = this
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        actionLogin.setOnClickListener {
            MainActivity.launch(requireContext())
        }
    }
}