package com.mstandroid.app.ui.auth

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mstandroid.app.components.form.LoginForm

class LoginViewModel : ViewModel() {
    val form = MutableLiveData(LoginForm())

    fun validate() {
        form.value?.apply {
            this.validate()
            Log.d("Validation", "Form Validation: ${this.isValid}")
        }
    }
}