package com.mstandroid.app.utils

import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.view.View
import androidx.appcompat.view.menu.MenuView
import androidx.core.view.children
import com.google.android.material.navigation.NavigationView
import com.mstandroid.app.R

class CustomNavigationView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : NavigationView(context, attrs, defStyleAttr) {

    private var footerLayoutId = View.NO_ID
    private var mFooterView: View? = null

    init {
        val a = context.obtainStyledAttributes(attrs, R.styleable.CustomNavigationView)
        try {
            footerLayoutId =
                a.getResourceId(R.styleable.CustomNavigationView_footerViewId, View.NO_ID)
        } finally {
            a.recycle()
        }
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        ensureView()
    }

    override fun onMeasure(widthSpec: Int, heightSpec: Int) {
        super.onMeasure(widthSpec, heightSpec)
        ensureView()
        val menuView = getMenuView() ?: return
        val lp = menuView.layoutParams as? MarginLayoutParams ?: return
        lp.bottomMargin = mFooterView?.measuredHeight ?: 0
        menuView.layoutParams = lp
    }

    private fun ensureView() {
        if (mFooterView == null)
            mFooterView = findViewById(footerLayoutId)
        val lp = mFooterView?.layoutParams as? LayoutParams ?: return
        lp.gravity = Gravity.BOTTOM
        mFooterView?.layoutParams = lp
    }

    private fun getMenuView(): View? {
        children.forEach {
            if (it is MenuView)
                return it
        }
        return null
    }

    fun getFooterView(): View? {
        return mFooterView
    }
}