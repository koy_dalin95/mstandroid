package com.mstandroid.app.components.form

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.Observable
import com.mstandroid.app.BR
import com.mstandroid.app.components.form.field.TextField

class LoginForm : BaseObservable() {

    val email = TextField()
    val password = TextField()

    private val obsFieldChange = object : Observable.OnPropertyChangedCallback() {
        override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
            notifyPropertyChanged(BR.loginEnable)
        }
    }

    init {
        email.addEmailValidator()
        password.addPasswordValidator()

        email.addOnPropertyChangedCallback(obsFieldChange)
        password.addOnPropertyChangedCallback(obsFieldChange)
    }

    @Bindable
    fun isLoginEnable(): Boolean {
        return isValid
    }

    val isValid: Boolean
        get() {
            return email.isValid() && password.isValid()
        }

    fun validate() {
        email.validate()
        if (!email.isValid()) {
            return
        }

        password.validate()
        if (!password.isValid()) {
            return
        }
    }
}