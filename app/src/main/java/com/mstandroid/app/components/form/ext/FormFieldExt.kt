package com.mstandroid.app.components.form.ext

import androidx.databinding.BindingAdapter
import com.google.android.material.textfield.TextInputLayout
import com.mstandroid.app.components.base.BaseField

@BindingAdapter("bindField")
fun TextInputLayout.bindField(field: BaseField<*>?) {
    this.isErrorEnabled = field == null || !field.isValid()
    this.error = field?.error
}