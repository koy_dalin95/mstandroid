package com.mstandroid.app.components.base

import androidx.annotation.CallSuper
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import com.mstandroid.app.BR

abstract class BaseField<T> : ObservableField<T>() {
    interface PropertyValidator<T> {
        fun getName(): String
        fun isValid(value: T?): Boolean
    }

    private val mValidators = LinkedHashMap<PropertyValidator<T>, String?>()

    open fun addValidator(errorText: String?, onValidator: (T?) -> Boolean) {
        val key = mValidators.keys.find { it.getName() == "_validator" }
        if (key != null) return

        val validator = object : PropertyValidator<T> {
            override fun isValid(value: T?): Boolean {
                return onValidator.invoke(value)
            }

            override fun getName(): String = "_validator"
        }
        mValidators[validator] = errorText
    }

    fun addValidator(errorText: String?, validator: PropertyValidator<T>) {
        val key = mValidators.keys.find { it.getName() == validator.getName() }
        if (key != null) return
        mValidators[validator] = errorText
    }

    fun removeValidator(name: String? = null) {
        val key = mValidators.keys.find { it.getName() == name }
        if (key != null) {
            mValidators.remove(key)
        } else {
            mValidators.clear()
        }
    }

    override fun set(value: T) {
        super.set(value)
        checkValidation()
    }

    @get:Bindable
    var error: String? = null
        set(value) {
            field = value
            notifyPropertyChanged(BR.error)
        }

    fun isValid(): Boolean {
        mValidators.forEach { entry ->
            if (!entry.key.isValid(get())) {
                return false
            }
        }
        return true
    }

    fun validate() {
        checkValidation()
    }

    private fun checkValidation() {
        mValidators.forEach { entry ->
            if (!entry.key.isValid(get())) {
                this.error = entry.value
                return
            }
        }
        this.error = null
    }

    @CallSuper
    open fun resetError() {
        this.error = null
    }
}