package com.mstandroid.app.components.form.field

import com.mstandroid.app.components.base.BaseField

class NumberField : BaseField<Number>() {
    fun addZeroValidator(errorText: String? = null) {
        addValidator(errorText ?: "Field must be not zero!") {
            when (it) {
                is Int -> it > 0
                is Float -> it > 0
                is Long -> it > 0
                is Double -> it > 0
                else -> false
            }
        }
    }
}