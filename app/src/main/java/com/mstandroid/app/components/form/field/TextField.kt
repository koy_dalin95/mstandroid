package com.mstandroid.app.components.form.field

import android.util.Patterns
import com.mstandroid.app.components.base.BaseField

class TextField : BaseField<String?>() {
    fun addEmptyValidator(errorText: String? = null): TextField {
        val emptyValidator = object : PropertyValidator<String?> {
            override fun isValid(value: String?): Boolean {
                return !value.isNullOrEmpty()
            }

            override fun getName(): String {
                return "empty_validator"
            }
        }

        addValidator(errorText, emptyValidator)
        return this
    }

    fun addRangeLengthValidator(
        errorText: String? = null,
        minLength: Int,
        maxLength: Int
    ): TextField {
        val rangeValidator = object : PropertyValidator<String?> {
            override fun isValid(value: String?): Boolean {
                val length = value?.trim {
                    false
                }?.length ?: 0

                return if (maxLength == -1) {
                    length >= minLength
                } else {
                    length in minLength..maxLength
                }
            }

            override fun getName(): String {
                return "range_validator"
            }
        }

        addValidator(errorText, rangeValidator)
        return this
    }

    fun addMinLengthValidator(errorText: String? = null, minLength: Int): TextField {
        return addRangeLengthValidator(errorText, minLength, -1)
    }

    fun addMaxLengthValidator(errorText: String? = null, maxLength: Int): TextField {
        return addRangeLengthValidator(errorText, 0, maxLength)
    }

    fun addPasswordValidator(errorText: String? = null) {
        addEmptyValidator(errorText ?: "Password can't be empty")
        addMinLengthValidator(
            errorText ?: "Password must be at-least 6 characters long",
            6
        )
    }

    /**
     * Validates email addresses based on Android's
     */
    fun addEmailValidator(errorText: String? = null) {
        addEmptyValidator(errorText ?: "Email can't be empty")
    }

    fun addNumberPhoneValidator(errorText: String? = null) {

    }
}