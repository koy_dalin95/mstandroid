package com.mstandroid.app.components.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment

abstract class BaseBindFragment<B : ViewDataBinding>(@LayoutRes private val layoutId: Int) :
    Fragment() {

    final override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = DataBindingUtil.inflate<B>(inflater, layoutId, container, false)
        checkNotNull(binding) {
            "Layout must be bind_able!"
        }
        onBindingView(binding)
        return binding.root
    }

    abstract fun onBindingView(binding: B)
}