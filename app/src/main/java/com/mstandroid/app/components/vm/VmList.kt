package com.mstandroid.app.components.vm

import com.promix.baelui.bind.base.VmBase

class VmList(title: String) : VmBase<String?>(title) {
    override fun getUnique(): String {
        return model.toString()
    }

    val title: String?
        get() = model
}